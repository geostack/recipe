#!/bin/bash

INSTALL_DIR=`pwd`/miniconda3

if [[ -f Miniconda3-latest-Linux-x86_64.sh ]];then
    rm Miniconda3-latest-Linux-x86_64.sh
elif [[ -f Miniconda3-latest-MacOSX-x86_64.sh ]];then
    rm Miniconda3-latest-MacOSX-x86_64.sh
fi

# download Miniconda3 from Anaconda website
if [[ `uname` == "Darwin" ]];then
    wget https://repo.anaconda.com/miniconda/Miniconda3-latest-MacOSX-x86_64.sh
elif [[ `uname` == "Linux" ]];then
    wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh
fi
# install miniconda3

if [[ `uname` == "Darwin" ]];then
    bash Miniconda3-latest-MacOSX-x86_64.sh -b -s -p ${INSTALL_DIR}
elif [[ `uname` == "Linux" ]];then
    bash Miniconda3-latest-Linux-x86_64.sh -b -s -p ${INSTALL_DIR}
fi

eval "$("$INSTALL_DIR"/bin/conda shell.bash hook)"

# install numpy in conda install

conda install -y numpy cython

# # download compiled geostack package
# fileid="1O21evtDcUV7YgH-Tm9gn0k1Ez4As_5KM"
# filename="geostack-alpha-py37h39e3cac_1.tar.bz2"
# curl -c ./cookie -s -L "https://drive.google.com/uc?export=download&id=${fileid}" > /dev/null
# curl -Lb ./cookie "https://drive.google.com/uc?export=download&confirm=`awk '/download/ {print $NF}' ./cookie`&id=${fileid}" -o ${filename}

# build geostack package
conda install -c conda-forge -y conda-build conda-smithy conda-verify
conda build --no-activate --no-anaconda-upload --no-include-recipe .

# install geostack in the new conda installation

#if [[ -f ${filename} ]];then
#    conda install ${filename}
#else
#    file_path=`find $INSTALL_DIR/conda-bld/ -type f -name "geostack*.tar.bz2" | tail -1`
#    cp $file_path `basename $file_path`
#    conda install $filename
#fi
