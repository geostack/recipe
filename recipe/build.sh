#!/bin/bash

mkdir -p build
pushd build

# configure geostack

cmake .. \
    -DBUILD_APPS=false \
    -DUSE_TILE_CACHING=true \
    -DUSE_THREADED_TILE_CACHING=true \
    -DBUILD_SHARED=true \
    -DCMAKE_BUILD_TYPE=Release \
    -DCMAKE_INSTALL_PREFIX=${PREFIX} \
    -DOpenCL_FLOAT_TYPE=float

# build geostack
cmake --build . --config Release -- -j${CPU_COUNT}

# install geostack
cmake --build . -- install

popd

mkdir -p pybuild
pushd pybuild

if [[ `uname` == 'Darwin' ]];then
    export CFLAGS='-mmacosx-version-min=10.7 -std=c++11 -stdlib=libc++'
fi

# configure python bindings for geostack

cmake ../bindings \
    -DCMAKE_INSTALL_PREFIX=${PREFIX} \
    -DGeostack_DIR=${PREFIX}/lib \
    -DCMAKE_BUILD_TYPE=Release \
    -DPYTHON=${PYTHON} \
    -DCYTHON_BUILD_FLAG=true

cd ./python/src

# build python bindings
${PYTHON} setup.py build_ext
${PYTHON} -m pip install --no-deps --ignore-installed .

cd ../../
