:: use local build folder
mkdir %SRC_DIR%\build
cd %SRC_DIR%\build

set BUILD_TYPE=Release

:: configure, use 64-bit build
cmake -G "Visual Studio 15 2017 Win64" %SRC_DIR% ^
    -DCMAKE_INSTALL_PREFIX:PATH="%LIBRARY_PREFIX%" ^
    -DCMAKE_BUILD_TYPE:STRING=%BUILD_TYPE% ^
    -DBUILD_APPS=false ^
    -DBUILD_SHARED=true ^
    -DUSE_TILE_CACHING=true ^
    -DUSE_THREADED_TILE_CACHING=true ^
    -DOpenCL_FLOAT_TYPE=float
if errorlevel 1 exit \b 1

:: build
cmake --build . --config %BUILD_TYPE%
if errorlevel 1 exit \b 1

:: install
cmake --build . --config %BUILD_TYPE% --target install
if errorlevel 1 exit \b 1

cd %SRC_DIR%

:: use build directory for python bindings
mkdir %SRC_DIR%\pybuild
cd %SRC_DIR%\pybuild

:: Configure python bindings for geostack
cmake %SRC_DIR%\bindings ^
    -DCMAKE_INSTALL_PREFIX:PATH="%LIBRARY_PREFIX%" ^
    -DGeostack_DIR:PATH="%LIBRARY_PREFIX%"/lib ^
    -DCMAKE_BUILD_TYPE=%BUILD_TYPE% ^
    -DPYTHON=$PYTHON ^
    -DCYTHON_BUILD_FLAG=true
if errorlevel 1 exit \b 1

cd python\src

:: build python bindings
%PYTHON% setup.py build_ext --plat-name win-amd64
%PYTHON% -m pip install . -vv
if errorlevel 1 exit \b 1

cd %SRC_DIR%
