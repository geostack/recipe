mkdir miniconda

wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Windows-x86_64.exe `
	-OutFile Miniconda3-latest-Windows-x86_64.exe

Start-Process .\Miniconda3-latest-Windows-x86_64.exe `
	-ArgumentList "/S /AddToPath=0 /RegisterPython=0 /InstallationType=JustMe /D=C:\Users\gar305\Documents\miniconda" `
	-wait -NoNewWindow

C:\Users\gar305\Documents\miniconda\Scripts\activate.bat

conda config --add channels conda-forge
conda install -c conda-forge conda-smithy conda-build conda-verify
conda build --no-activate --no-upload --no-include-recipe .