Description
===========

For building conda package from geostack code base, following steps are needed.

* Install [miniconda](https://docs.conda.io/en/latest/miniconda.html)
* Clone [geostack_recipe](https://gitlab.com/geostack-library/geostack_recipe) directory
* Install conda package building tools i.e. [conda-build](https://docs.conda.io/projects/conda-build/en/latest/) within the conda environment
* Build package using `conda-build`
* Install package built in previous step

After installing Miniconda, clone the repository.

```
git clone https://gitlab.com/geostack-library/geostack_recipe.git
```

Thereafter, install tools for building package with conda

```
conda install -c conda-forge conda-build conda-smithy conda-verify
```

and finally, build the geostack package and its python bindings

```
cd geostack_recipe
conda build --no-activate .
```

If these steps complete without giving any crazy error, then there should be 
compiled package stored in the **conda-bld** directory within the python 
installation. Usually, the package is located under the directory named 
after the platform used for building the package.
